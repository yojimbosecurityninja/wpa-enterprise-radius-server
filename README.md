<h1 align="center">
  <a href="https://github.com/yojimbo/wpa-enterprise-radius-server">
    <!-- Please provide path to your logo here -->
    <img src="docs/images/logo.svg" alt="Logo" width="100" height="100">
  </a>
</h1>

<div align="center">
  WPA-Enterprise Radius Server
  <br />
</div>

<div align="center">
<br />

</div>

<details open="open">
<summary>Table of Contents</summary>

- [About](#about)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)

</details>

---

## About

This is a Kali Linux Vagrant box setup to help users walk throuth my blog post
about setting up a freeradius server.

### Built With

- Vagrant

## Getting Started

### Prerequisites

You will need to install Vagrant and Virtualbox if you do not already have it.

### Installation

I will not cover installing Virtualbox or Vagrant here. A simple Google search
will provide the information you need.

## Usage

To get started bring up the virtual machine with the following:

```shell
vagrant up
````
Next, SSH into the box as seen below:

```shell
vagrant ssh
```

To stop the virtual machine:

```shell
vagrant hault
```

If it all goes wrong destroy the VM and start over:

```shell
vagrant destroy -f
```
